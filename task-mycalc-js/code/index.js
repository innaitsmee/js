const [...numbers] = document.querySelectorAll(".black");
const calc = {
    value1: "",
    value2: "",
    operation: "",
    save: 0,
    arr: []
}

let click = false,
    operation = false,
    calcArr = [],

    //якщо один з операндів === "", даємо йому значення 0 щоб не було NaN при воконанні операції
    zero = (value) => {
        if (calc[value] === "") {
            calc[value] = "0";
        }
    },
    
    unblocked = function () {
        numbers.forEach((e) => {
            if (e.value === "." && e.disabled === true) {
                e.disabled = false;
            }
        })
    },
   
    calcValue = function (value, e) {
        //робимо щоб нуль був попереду тільки 1
        if (calc[value] === "0" && e.value !== ".") {
            calc[value] = "";
        }
        //робимо щоб при кліку на крапку спереду з*являвся нуль   
        else if (calc[value] === "" && e.value === ".") {
            calc[value] = "0";
            //блокуємо крапку після кліку, щоб можна було тільки раз клікнути по ній
            e.disabled = true;
        } else if (calc[value] !== "" && e.value === ".") {
            e.disabled = true;
        }
        calc[value] += e.value;
    },

    add = (num1, num2) => {
        return getResult(num1, num2, parseFloat(num1) + parseFloat(num2));
    },

    sub = (num1, num2) => {
        return getResult(num1, num2, parseFloat(num1) - parseFloat(num2));
    },

    mul = (num1, num2) => {
        return getResult(num1, num2, parseFloat(num1) * parseFloat(num2));
    },

    div = (num1, num2) => {
        if (num2 === "0") {
            return "error";
        }
        return getResult(num1, num2, parseFloat(num1) / parseFloat(num2));
    },

    //додала щоб не було сильно довгих чисел в результаті при операціях з дробовими числами
    getResult = (num1, num2, result) => {
        if (/\.\d{2,}/.test(num1) || /\.\d{2,}/.test(num2)) {
            return result.toFixed(2);
        } else if (/\.\d{1}/.test(num1) && /\.\d{1}/.test(num2)) {
            return result.toFixed(1);
        } else return result;
    },

    show = (value, el) => {
        el.value = value;
    },

    clear = function() {
        calc.value1 = "";
        calc.value2 = "";
        calc.operation = "";
        unblocked();
    },

    pushElem = (value) => {
        unblocked();
        calc.save += parseFloat(calc[value]);
        calc.arr.push(parseFloat(calc[value]));
    },

    popElem = () => {
        calc.save -= calc.arr[calc.arr.length - 1];
        calc.arr.pop();
    };

function calculate(operation, op1, op2) {
    switch (operation) {
        case "+": return add(op1, op2); 
        case "-": return sub(op1, op2); 
        case "*": return mul(op1, op2); 
        case "/": return div(op1, op2);
    }
}

function fillMemory() {
    if (calc.value1 !== "" && calc.value2 !== "") {
        calc.save += calculate(calc.operation, calc.value1, calc.value2);
        //додаємо кожне число, що додали в пам*ять, в масив, 
        //щоб потім можна було видалити числа в порядку, в якому вони додавались
        calc.arr.push(calculate(calc.operation, calc.value1, calc.value2));
    } else if (calc.value1 === "") {
        pushElem("value2");
    } else if (calc.value2 === "") {
        pushElem("value1");
    }
    clear();
}

function clearMemory() {
//при 1 кліку дані з пам*яті виводяться на екран    
    if (click === false) {
        click = true;
//при повторному кліку очищаємо пам*ять та масив з доданими числами
    } else {
        calc.save = 0;
        calc.arr = [];
        click = false;
    }
    calc.value1 = calc.save;
}

window.addEventListener("DOMContentLoaded", () => {
    const btn = document.querySelector(".keys"),
        display = document.querySelector(".display > input"),
        res = document.querySelector(".orange");

    btn.addEventListener("click", function(e) {
        //перевіряємо чи містить об*єкт події клас, в якому записані числа і крапка
        //і виключаємо "C"
        if (e.target.classList.contains("black") && e.target.value !== "C") {

            //перевіряємо чи є значення в value2 і calc.operation. якшо немає, то записуємо value1
            if (calc.value2 === "" && calc.operation === "") {
                calcValue("value1", e.target);
                show(calc.value1, display);
            } else {
                show("", display);
                calcValue("value2", e.target);
                show(calc.value2, display);
            }
            calcArr.push(e.target.value);
        }
        //визначаємо чи є на кнопці клас, що містить оператора і записуємо в об*єкт
        if (e.target.classList.contains("pink")) {
            unblocked();
            calc.operation = e.target.value;
            
            //перевірка щоб не можна було додати підряд декілька операторів
            if (calc.operation) {
                if (operation === false) {
                    calcArr[1] = calc.operation;
                    operation = true;
                } 
            }
            //беремо дані з масива щоб була можливість робити підряд операції +-/*   
            if (calcArr.length === 3) {

                if (/\+/.test([...calcArr])) {
                     calc.value1 = calculate("+", calcArr[0], calcArr[2]);
                 
                } else if (/\-/.test([...calcArr])) {
                     calc.value1 = calculate("-", calcArr[0], calcArr[2]);
                
                } else if (/\*/.test([...calcArr])) {
                     calc.value1 = calculate("*", calcArr[0], calcArr[2]);
                    
                } else if (/\//.test([...calcArr])) {
                     calc.value1 = calculate("/", calcArr[0], calcArr[2]);
                };

                calcArr[0] = calc.value1;
                calcArr[1] = calc.operation;
                calcArr.pop();
                calc.value2 = "";
                show(calc.value1, display);
            } 
            res.disabled = false;
        } 
        //записуємо результат і виводимо на екран
        if (e.target.value === "=") {
        //щоб не було на екрані NaN якщо не введене одне з чисел    
            zero("value1");
            zero("value2");

            if (calc.operation === "") {
                calc.operation = "+";
            }
            calc.value1 = `${calculate(calc.operation, calc.value1, calc.value2)}`;
            show(calc.value1, display); 
            calc.value2 = "";
            calc.operation = "";
        } 
        //очищуємо калькулятор
        if (e.target.value === "C") {
            clear();
            show("0", display);
            res.disabled = true;
            click = false;
            calcArr = [];
        }
        //зберігаємо/видаляємо дані з пам*яті калькулятора
        if (e.target.value === "m-" || e.target.value === "m+") {
            show(e.target.value, display);
            display.setAttribute("id", "left"); //додала id щоб текст відображався зліва

            //зберігаємо дані в пам*ять. Дані в пам*яті сумуються, якщо числа зберігались декілька разів.    
            if (e.target.value === "m+") {
                fillMemory();
            }
            //віднімаємо по черзі числа які додавались в пам*ять і видаляємо їх з масива
            else if (e.target.value === "m-") {
                popElem();
            }

        } else display.removeAttribute("id", "left");
        //виводимо дані з пам*яті на дісплей
        if (e.target.value === "mrc") {
            clearMemory();
            if (calc.save === 0) {
                calc.value1 = "";
                show("0", display)
            } else show(calc.value1, display);
        }
    })
})