/*
Создай класс, который будет создавать пользователей с именем и фамилией. Добавить к классу метод для вывода имени 
и фамилии
*/

class User {
    constructor(name, sname) {
        this.name = name;
        this.sname = sname;
    }
    
    show = () => {
       document.querySelector(".user").textContent = `${this.name} ${this.sname}`;
    }
}

const user = new User("Inna", "Ponomarenko");
user.show();

/*
Создай список состоящий из 4 листов. Используя джс обратись к 2 li и с использованием навигации по DOM 
дай 1 элементу синий фон, а 3 красный
*/

const li = document.querySelector(".list > li:nth-child(2)");
li.previousElementSibling.classList.add("blue");
li.nextElementSibling.classList.add("red");

/*
Создай див высотой 400 пикселей и добавь на него событие наведения мышки. При наведении мышки выведите текстом
координаты, где находится курсор мышки
*/

const div = document.createElement("div");

document.querySelector(".list").after(div);
div.addEventListener("mouseover", (e) => {
    div.textContent = `X: ${e.clientX}, Y: ${e.clientY}`;
})

/*
Создай кнопки 1,2,3,4 и при нажатии на кнопку выведи информацию о том какая кнопка была нажата
*/

const [...buttons] = document.querySelectorAll("button"),
result = document.querySelector(".result");

buttons.forEach(item => {
    item.addEventListener("click", () => {
        result.textContent = `Нажата кнопка ${item.textContent}`;
    })
})

/*
Создай див и сделай так, чтобы при наведении на див, див изменял свое положение на странице
*/

const movedDiv = document.querySelector(".moved");

let coordinate = (min, max) => {
    return Math.floor(Math.random() * (max - min));
}

movedDiv.addEventListener("mouseover", (e) => {
    e.target.style.top = `${coordinate(0, 80)}%`;
    e.target.style.left = `${coordinate(0, 80)}%`;
})

/*
Создай поле для ввода цвета, когда пользователь выберет какой-то цвет сделай его фоном body
*/

const color = document.querySelector(".color");

color.addEventListener("change", (e) => {
    document.body.style.backgroundColor = e.target.value;
})

/*
Создай инпут для ввода логина, когда пользователь начнет Вводить данные в инпут выводи их в консоль
*/

const login = document.querySelector(".login");
login.addEventListener("input", (e) => console.log(e.target.value));

/*
Создайте поле для ввода данных поле введения данных выведите текст под полем
*/

const input = document.querySelector(".enter-info");

input.addEventListener("change", (e) => {
    document.querySelector("input + div").textContent = `${e.target.value}`;
});