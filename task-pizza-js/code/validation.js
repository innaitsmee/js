//валідація
const validation = document.querySelector(".grid"),
[user, tel, mail, ...grid] = document.querySelectorAll(".grid > input"),
labels = [user, tel, mail];

let valid = {
    name: false,
    tel: false,
    mail: false
};

let isValid = (elem, value, key, pattern, val) => {
    pattern.test(value)
    if (pattern.test(value)) {
        order[key] = value;
        elem.classList.remove("error");
        elem.classList.add("success");
        valid[val] = true;
    } else {
        elem.classList.add("error");
        valid[val] = false;
    }
},
    
//перехід між інпутами за допомогою клавіатури
keyDown = (e) => {
    labels.forEach((elem, i) => {
        if (i < labels.length - 1) {
            if (e.target.name === elem.name) {
                labels[i + 1].focus();
            }
            if (e.target.name === "email") {
                goToFinalPage();
            }
        }
    })
},
    
goToFinalPage = () => {
    if (valid.name === valid.tel === valid.mail && valid.name === true) {
        window.location.href = "./thank-you.html";
    } else return;
};

validation.addEventListener("change", (e) => {

    labels.forEach((element) => {

        if (e.target.name === "name" && e.target.name === element.name) {
        isValid(element, element.value, "name", /[А-яіїєґ'-]/, "name");
        }
        if (e.target.name === "phone" && e.target.name === element.name) {
        isValid(element, element.value, "tel", /\+38\d{10}$/, "tel");
        }
        if (e.target.name === "email" && e.target.name === element.name) {
        isValid(element, element.value, "mail", /[a-z0-9-_.]+@[a-z]+\.[a-z]{2,4}$/, "mail");
        } 
    })
})

validation.addEventListener("keydown", (e) => {
    if (e.keyCode === 13) {
        e.target.blur();
        keyDown(e);
    }
})

//підтвердити замовлення
const submit = document.querySelector("input[type='button']");
submit.addEventListener("click", goToFinalPage);

//відмінити замовлення
const reset = document.querySelector("input[type='reset']");

reset.addEventListener("click", () => {
    valid = {
    name: false,
    tel: false,
    mail: false,
    };

    labels.forEach(e => {
        e.classList.remove("success", "error");
    })
})