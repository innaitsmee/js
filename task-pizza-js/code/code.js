/*Створіть сайт конструктор піци 
Потрібно зробити так, щоб при виборі розміру піци ціна у залежності від коржа змінювалась.
Також користувач має мати змогу додавати топінги та соуси у необмеженій кількості на корж, 
топінги та соуси повинні показуватись на коржі та додатись до списку інгредієнтів та 
враховуватись у вартість піци.
Поле форми з даними користувача  перевірити на правильність вводу, 
кнопка отримати знижку має втікати від користувача*/

const sizes = [small = {name: "Маленька", price: 50},
               medium = {name: "Середня", price: 100},
               large = { name: "Велика", price: 150 }],
    
sauces = [ketchup = {name: "Кетчуп", price: 5},
         bbg = {name: "BBQ", price: 10},
         ricotta = { name: "Рікотта", price: 15 }],

toppings = [cheese = {name: "Сир звичайний", price: 12},
           feta = {name: "Сир фета", price: 14},
           mozzarella = {name: "Моцарелла", price: 16},
           veal = {name: "Телятина", price: 18},
           tomato = {name: "Помідори", price: 6},
           mushrooms = {name: "Гриби", price: 8}],

order = {
size: "",
sauce: [],
topping: [],
sizePrice: 0,
saucesPrice: 0,
toppingPrice: 0,
resultPrice: 0
},
 
pizzaSize = document.querySelector("#pizza"),
result = document.createElement("span"),
//масив радіоперемикачів з розміром піци      
[...radioValue] = document.querySelectorAll(".radioIn"),
//масив src топінгів і соусів
[a, b, c, ...toppingsSRC] = document.querySelectorAll(".draggable"),
saucesSRC = [a, b, c],
draggable = [...saucesSRC, ...toppingsSRC],
//масиви, які будуть наповнюватись картинками при виборі інгредієнтів (потрібні для видалення картинок)    
dragSauces = [],
dragToppings = [];

let chooseSize = (e) => {
    radioValue.forEach((element, i) => {
        if (e.target.value === element.value) {
            order.size = sizes[i];
        }
    })
},
 
chooseIngredients = (array, key, arr, e) => {
    array.forEach((element, i) => {
        if (e.src === element.src) {
            order[key].push(arr[i]);
            
            addIngredients(key, element);
            deleteIngredients(key);
        }
    })
}, 
//додаємо соуси/топінги на сторінку
addIngredients = (key, item) => {
    if (key === "sauce") {
        createContent("sauce", ".sauces > p");
        dragSauces.push(item);
    } else {
        createContent("topping", ".topings > p");
        dragToppings.push(item);
    }
},
//створюємо діви з кнопкою, щоб можна було потім видаляти зі сторінки
createContent = (key, elem) => {
    const div = document.createElement("div"),
        btn = document.createElement("button");
    btn.classList.add("btn");
    btn.textContent = "X";
    for (let element of order[key]) {
        div.textContent = element.name;
        document.querySelector(elem).append(div);
        div.append(btn);
    }
},

deleteIngredients = (key) => {
    if (key === "sauce") {
        deleteContent("sauce", ".sauces div", dragSauces);
    } else deleteContent("topping", ".topings div", dragToppings);
},

deleteIMG = (index, dragArr) => {
//дропнуті картинки
const [first, ...dropedIMG] = document.querySelectorAll(".table > img");
//використала звичайний цикл, щоб можна було використати return і не видалялись зайві картинки
    for (let i = 0; i < dropedIMG.length; i++){
        if (dropedIMG[i].src === dragArr[index].src) {
            
            dragArr[index].draggable = true;
            dropedIMG[i].remove();
            dragArr.splice(index, 1);
            dropedIMG.splice(i, 1);
            return;
        }
    }
},
//видаляємо зі сторінки додані соуси/топінги 
deleteContent = (key, selector, dragArr) => {
    const [...buttons] = document.querySelectorAll(".btn");
    buttons.forEach(elem => {
        elem.addEventListener("click", (e) => {
            //ingredientsDiv - діви, що з*являються в топінгах і соусах біля ціни
            const [...ingredientsDiv] = document.querySelectorAll(selector);

            ingredientsDiv.forEach((element, index) => {
                if (element === e.target.parentElement) {    //e.target - натиснутий Х
                    element.remove();
                    order[key].splice(index, 1);

                    deleteIMG(index, dragArr);
                }    
            })
        })
        elem.addEventListener("click", calculatePrice);
        elem.addEventListener("click", showPrice);
    })
},

sizePrice = () => {
    if (order.size === "") {
        order.size = sizes[2];
        order.sizePrice = sizes[2].price;
    } else order.sizePrice = order.size.price;
    
    return order.sizePrice;
},

ingredientsPrice = (price, key) => {
    order[price] = 0;
    order[key].forEach( elem => {
        order[price] += elem.price;
    })
    return order[price];
},

calculatePrice = () => {
    order.resultPrice = sizePrice() + ingredientsPrice("saucesPrice", "sauce") + ingredientsPrice("toppingPrice", "topping");
},

showPrice = () => {
    result.textContent = ` ${order.resultPrice} грн`;
    document.querySelector(".price > p").append(result);
};

pizzaSize.addEventListener("click", chooseSize);
pizzaSize.addEventListener("click", calculatePrice);
pizzaSize.addEventListener("click", showPrice);

//drag and drop
const main = document.querySelector(".table");
    
let dragStart = (e) => {
    e.dataTransfer.setData("id", e.target.id);
},

drop = (e) => {
    let dropedItem = e.dataTransfer.getData("id"),
        copy = document.getElementById(dropedItem).cloneNode(); 
    document.getElementById(dropedItem).className = ".table > img";
    main.append(copy);
};

let z = 0;  //z для створення zIndex, щоб інгредієнти наслоювались в правильному порядку
draggable.forEach(item => {
    item.style.zIndex = z;
    z++;
    if (item.id.includes("moc")) {
        item.style.zIndex = z + 6;
    } else item.style.zIndex = z;

    item.addEventListener("dragstart", dragStart);
    item.addEventListener("dragend", (e) => {
        if (e.target.className === "draggable") {
            return;
        } else {
            chooseIngredients(toppingsSRC, "topping", toppings, e.target);
            chooseIngredients(saucesSRC, "sauce", sauces, e.target);
            e.target.className = "draggable";
            e.target.draggable = false;
        }   
    }); 
    item.addEventListener("dragend", calculatePrice);
    item.addEventListener("dragend", showPrice); 
});

main.addEventListener("dragover", (e) => {
    e.preventDefault();
});

main.addEventListener("drop", drop);

//щоб при завантаженні сторінки відображалась ціна за велику піцу, так як вона вибрана за замовчуванням
window.addEventListener("DOMContentLoaded", calculatePrice);
window.addEventListener("DOMContentLoaded", showPrice);