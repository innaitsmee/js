    function Human (name, age){
        this.name = name;
        this.age = age;
    }

    let humans = [];

    humans[0] = new Human("Masha", 21);
    humans[1] = new Human("Sasha", 27);
    humans[2] = new Human("Dasha", 20);
    humans[3] = new Human("Natasha", 18);
    humans[4] = new Human("Pasha", 31);

    for (let i = 0; i < humans.length; i++) {
        
    document.write(`Human ${[i + 1]}: ${humans[i].name}, ${humans[i].age} <br>`); //використала document.write, бо getElementById не виводив в стовбець
    }

    document.write(`<br> <hr> <br>`);

    function sortArr (array, condition) {

        if (condition === "grow") {

            array.sort((a, b) => {
                return a.age > b.age ? 1 : -1;
            })

        }

        else if (condition === "reduce") {

            humans.sort((a, b) => {
                return a.age < b.age ? 1 : -1;
            })

        }

        for (let i = 0; i < array.length; i++) {
            document.write(`Human ${[i + 1]}: ${humans[i].name}, ${humans[i].age} <br>`);
        }

    }

    sortArr(humans, "grow");

    document.write(`<br> <hr> <br>`);

    sortArr(humans, "reduce");
        
    document.write(`<br> <hr> <br>`);
    
//завдання 2
    
function Human2(name, birthYear, workExperience, langugageLevel) {
    this.name = name;
    this.birthYear = birthYear;  
    this.workExperience = workExperience;
    this.langugageLevel = langugageLevel; 
}

Human2.position = "Marketing Manager";

Human2.prototype.age = function () {
    this.age = 2022 - this.birthYear; 
}

Human2.prototype.experience = function () {

    if (this.workExperience >= 3) {
        return `Надішліть, будь ласка, Ваше портфоліо на пошту lalala@gmail.com`
    }

    else {
        return `Наразі, на жаль, розглядаємо кандидатів лише з досвідом роботи від 3 років.`

    }
}


Human2.prototype.show = function (callback) {

    if (this.age >= 20 && this.age < 80) {

        if (this.langugageLevel === "1" || this.langugageLevel === "2") {
            return `Добрий день, ${this.name}. 
            На жаль, Вашого рівня англійської наразі недостатньо. <br>`;
        } else {
        
            return `Добрий день, ${this.name}. ${this.experience()} <br>`;

        }
    
    }

    else {

        if (this.workExperience <= 3) {
            return `${this.name}, дякуємо за Ваш відгук.
            Однак, наразі розглядаємо кандидата іншої вікової категорії (20-80 років) та досвідом роботи більше 3 років.`
        }

        else {
            return `${this.name}, дякуємо за Ваш відгук. 
            Однак, наразі розглядаємо кандидата іншої вікової категорії (20-80 років).`
       
        }
    }

}

Human2.createArr = function () {

    let arr = [];
    
    for (let i = 0; i < 3; i++) {
        let human = new Human2(prompt("Вкажіть Ваше ім*я"), parseFloat(prompt("Вкажіть рік народження")), parseFloat(prompt("Вкажіть Ваш досвід роботи (загальну к-сть років)")), prompt("Вкажіть Ваш рівень англійської, де 1 - Elementary, 2 - Pre-Intermediate, 3 - Intermediate, 4 - Upper-Intermediate, 5 - Advanced"));
        
        if (human.workExperience < 3) {
            
            continue;
        }

        else (
            arr.push(human)
        )
    }

    return arr;  
}

Human2.sortArr = function (arr) {
    arr.sort((a, b) => {
        return a.workExperience > b.workExperience ? 1 : -1;
    });

    arr.sort((a, b) => {
        return a.langugageLevel < b.langugageLevel ? 1 : -1;
    })

    console.log(`Best candidate is ${arr[0].name}`)
    return arr;
}

console.log(Human2.sortArr(Human2.createArr()));

//калькулятор

function Calculator() {
    
}

Calculator.prototype.read = function (a, b) {
    this.op1 = a;
    this.op2 = b;
}

Calculator.prototype.sum = function () {
    return this.op1 + this.op2;
}

Calculator.prototype.mul = function () {
    return this.op1 * this.op2;
}

let calculate = new Calculator();

calculate.read(+prompt("a"), +prompt("b"));
calculate.sum();
calculate.mul();

