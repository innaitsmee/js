/*## Задание

Написать реализацию кнопки "Показать пароль".

#### Технические требования:
- В файле `index.html` лежит разметка для двух полей ввода пароля. 
- По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, 
  иконка меняет свой внешний вид.
- Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
- Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
- По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
- Если значения совпадают - вывести модальное окно (можно alert) с текстом - `You are welcome`;
- Если значение не совпадают - вывести под вторым полем текст красного цвета  `Нужно ввести одинаковые 
  значения`
- После нажатия на кнопку страница не должна перезагружаться
- Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.*/

let [...input] = document.querySelectorAll(".input-wrapper input"),
    [...icon] = document.querySelectorAll(".input-wrapper i"),
    eyeOpen = "fas fa-eye icon-password",
    eyeClose = "fas fa-eye-slash icon-password",
    errorText = false;

input.forEach(e => {
    e.onclick = function() {
    icon.forEach((e, i) => {
        if (e.className === eyeOpen) {
            input.forEach((elem, index) => {
                if (index === i) {
                    elem.type = "text";
                }
            })
        } 
    })
  }
})

icon.forEach(function(e, i) {
    e.onclick = function() {
        if (e.className === eyeOpen) {
            e.className = eyeClose;
            input.forEach((elem, index) => {
                if (index === i) {
                    elem.type = "password";
                }
            })
        } else {
            e.className = eyeOpen;
            input.forEach((elem, index) => {
                if (index === i) {
                    elem.type = "text";
                }
            })
        }
    }
})

function welcome() {
    return "You are welcome";
}

function error() {
    let error = document.createElement("p");
    error.textContent = `*Нужно ввести одинаковые значения`;
    error.style.color = "red";

    if (!errorText) {
        input[1].after(error);
        errorText = true;
    }
}

function getAnswer(event) {
    event.preventDefault(); //відключила перезавантаження сторінки
    
    if (input[0].value === input[1].value && input[0].value !== "") {
            if (errorText === true) {
            document.querySelector("p").remove();
        }
            setTimeout(function() {
            alert(welcome());
        }, 100);
          
    } else error(); 
}   

document.querySelector(".btn").onclick = getAnswer;

    
        
    
        






