//При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг".
//Данная кнопка должна являться единственным контентом в теле HTML документа,
//весь остальной контент должен быть создан и добавлен на страницу с помощью Javascript

//При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга.
//При нажатии на кнопку "Нарисовать" создать на странице 100 кругов(10 * 10) случайного цвета.
//При клике на конкретный круг - этот круг должен исчезать, при этом пустое место заполняться,
//то есть все остальные круги сдвигаются влево.

let click = false;

function createElem(elem, content = "", value = "") {
    let element = document.createElement(elem);
  
    if (content !== "") {
    element.textContent = content;
    } 

    if (value !== "") {
    element.value = value;
    } 

    return element;
}

function createButtons() {

    if (!click) {
        const input = createElem("input", undefined, 50); //undefined вказала бо контент тут відсутній
        input.className = "diametr";
        document.querySelector("#button").after(input);
        
        const button = createElem("input", undefined, "Намалювати");
        button.type = "button";
        input.after(button);
        button.onclick = createRound;
    
        const main = createElem("div"); //додала щоб кружки малювались з нової стрічки
        main.className = "main";
        button.after(main);
        click = true; //визначаємо чи був клік, щоб не з*являлись заново поле вводу і ще одна кнопка "Намалювати" на сторінці
    }
}

function createRound() {
   
   let count = 100,
   diametr = `${parseFloat(document.querySelector(".diametr").value)}px`;
    
    for (let i = 0; i < count; i++) {

    const round = createElem("div");
    round.className = "round";
    round.style.height = diametr;
    round.style.width = diametr;
    round.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`;
    document.querySelector(".main").append(round);
    round.onclick = deleteRound;    
    }
}

function deleteRound() {
   return document.querySelector(".round").remove();
}

document.querySelector("#button").onclick = createButtons;
