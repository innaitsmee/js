//Створити клас Car , Engine та Driver.
//Клас Driver містить поля - ПІБ, стаж водіння.
//Клас Engine містить поля – потужність, виробник.
//Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver,
//мотор типу Engine. Методи start(), stop(), turnRight(), turnLeft(),
//які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч".А також метод toString(),
//який виводить повну інформацію про автомобіль, її водія і двигуна.
//
//Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
//Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.

class Car {
    constructor(marka, carClass, weight, engine, driver) {
        this.marka = marka;
        this.carClass = carClass;
        this.weight = weight;
        this.engine = engine;
        this.driver = driver;
    }

    start() {
        return "Поїхали";
    }

    stop() {
        return "Зупиняємося";
    }

    turnRight() {
        return "Поворот праворуч";
    }

    turnLeft() {
        return "Поворот ліворуч";
    }

    toString() {
        for (let i in this) {
            if (typeof this[i] === "object") {
                for (let j in this[i]) {
                    console.log(j + ":" + this[i][j]);
                }
            } else console.log(i + ":" + this[i]);
        }
    }
}

class Engine {
    constructor(power, company) {
        this.power = power;
        this.company = company;
    }
}

class Driver {
    constructor(name, experience) {
        this.fullName = name;
        this.experience = experience;
    }
}

class Lorry extends Car {
    constructor(marka, carClass, weight, engine, driver, carrying) {
        super(marka, carClass, weight, engine, driver);
        this.carrying = carrying;
    }
}

class SportCar extends Car {
    constructor(marka, carClass, weight, engine, driver, speed) {
        super(marka, carClass, weight, engine, driver);
        this.speed = speed;
    }
}

/*
let car = new Car(prompt("car mark"),
    prompt("car class"), parseFloat(prompt("car weight")),
    new Engine(parseFloat(prompt("engine power")), prompt("engine company")),
    new Driver(prompt("driver fullname"), prompt("driver experience")));

console.log(car);
console.log(car.toString())

let sport = new SportCar(prompt("car mark"),
    prompt("car class"), parseFloat(prompt("car weight")),
    new Engine(parseFloat(prompt("engine power")), prompt("engine company")),
    new Driver(prompt("driver fullname"), prompt("driver experience")), prompt("speed"))

console.log(sport);
console.log(sport.toString())    
*/

