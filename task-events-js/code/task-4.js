/*
- При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. 
Це поле буде служити для введення числових значень
- Поведінка поля має бути такою:
- При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
- Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, 
в якому має бути виведений текст: 
. 
Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
- При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
- Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, 
під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється.
*/

const price = document.querySelector(".price"),
input = document.querySelector(".price > input");

let getElem = (el, content) => {
    const elem = document.createElement(el);
    elem.textContent = content;
    return elem;
}   

const span = getElem("span"),
btn = getElem("button", "X"),
error = getElem("div", `Please enter correct price`);

let errorPrice = (e) => {
    if (parseFloat(e.target.value) < 0) {
        e.target.classList.add("error");
        e.target.after(error);
        error.classList.add("error-text")
        e.target.classList.remove("green");
        span.remove();
        btn.remove();
    }
}

input.addEventListener("focus", (e) => {
    e.target.classList.remove("green", "error");
    error.remove();
    e.target.classList.add("success");
})

input.addEventListener("blur", (e) => {
    e.target.classList.remove("success");
})

input.addEventListener("change", (e) => {
    price.before(span);
    span.after(btn);
    span.textContent = e.target.value;
    e.target.classList.add("green");
    e.target.classList.remove("error");
    error.remove();

    if (e.target.value === "") {
        btn.remove();
        span.remove();
    }
})

input.addEventListener("change", errorPrice);

btn.addEventListener("click", () => {
    btn.remove();
    span.remove();
})

//validation
let validationPrice = (e) => {
    e.target.value = e.target.value.replace(/[^-0-9]+/, "");

    if (/-?\d-$/.test(e.target.value)) {
        e.target.value = e.target.value.replace(/-$/, "");
    }

    if (/^-{2}/.test(e.target.value)){
        e.target.value = e.target.value.replace(/-/, "");
    }
}

input.addEventListener("input", validationPrice);