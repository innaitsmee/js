/* Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість символів. 
Скільки символів має бути в інпуті, зазначається в атрибуті data-length. Якщо вбито правильну кількість, 
то межа інпуту стає зеленою, якщо неправильна – червоною.*/

const [...inputs] = document.querySelectorAll(".sec-task > input");
let dataLength = 1,
    
validation = (e) => {
    if (e.target.value.length === dataLength) {
        e.target.classList.add("success");
    } else e.target.classList.add("error");
}

inputs.forEach(elem => {
    elem.setAttribute("data-length", `${dataLength}`);
    elem.addEventListener("blur", validation);
    elem.addEventListener("focus", (e) => {
        e.target.classList.remove("success", "error");
    })
});