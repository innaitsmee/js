/* Використовуючи бібліотеку bootstrap створіть форму у якій запитати у користувача дані.
Ім'я, Прізвище (Українською)
Список з містами України 
Номер телефону у форматі +380XX XXX XX XX - Визначити код оператора та підтягувати логотип оператора. 
Пошта 
Якщо поле має помилку показати червоний хрестик  біля поля ❌,  якщо помилки немає показати зелену галочку ✅
Перевіряти дані на етапі втрати фокуса та коли йде натискання кнопки відправити дані */

import city from "./uacity.js";
import operators from "./operators.js";

const [...inputs2] = document.querySelectorAll(".third-task input"),
div = document.createElement("div"),
logo = document.createElement("img"),
[...btns] = document.querySelectorAll(".btn");

let userInfo = {};

//відсортувала масив, щоб був алфавітний порядок
const sortArr = city.map(elem => elem.city).sort();

sortArr.forEach(element => {
    const option = document.createElement("option");
    option.textContent = element;
    document.querySelector(".form-select").append(option);
})

//додавання лого при введенні номера телефона
let addLogo = (e) => {
    operators.forEach(item => {
        if (e.target.value.startsWith(`+38${item.code}`)) {
            
            div.classList.add("logo");
            e.target.classList.add("step-left");
            e.target.parentElement.prepend(div);
            div.append(logo);
            logo.setAttribute("src", item.img);
        }
    })
}

//видалення лого
let removeLogo = (e) => {
    if (e.target.value.length < 6) {
        logo.remove();
        div.classList.remove("logo");
        e.target.classList.remove("step-left");
    }
}

//додаємо автоматично пробіли при записі номера телефона
let addPhone = (e) => {
    if (e.target.value.length === 6 || e.target.value.length === 10 ||
        e.target.value.length === 13) {
        e.target.value += " ";
    }
    return e.target.value;
}

let isValid = (id, pattern, key, value) => {
    let span = document.querySelector(`#${id} + span`);
    
    if (pattern.test(value)) {
        span.innerHTML = "&#9989;";
        userInfo[key] = value.toLowerCase();
    } else {
        span.innerHTML = "&#10060;";
    }
}

let validate = (elem, id, pattern, key, value = elem.value) => {
    if (elem.id === id) {
        isValid(id, pattern, key, value);
    }
}

inputs2.forEach(elem => {
    if (elem.id === "inputPhone4") {
        elem.addEventListener("input", addPhone);
        elem.addEventListener("input", addLogo);
        elem.addEventListener("input", removeLogo);
    }

    elem.addEventListener("input", () => {
        document.querySelector(`#${elem.id} + span`).textContent = "";
    })

    elem.addEventListener("change", (e) => {
        validate(elem, "inputName", /^[А-яЇїІіЄєЙй'-]+$/, "name");
        validate(elem, "inputSname", /^[А-яЇїІіЄєЙй'-]+$/, "sname");
        validate(elem, "inputPhone4", /\+380\d{2} \d{3} \d{2} \d{2}/, "tel", addPhone(e));
        validate(elem, "inputEmail4", /^[a-z_.-]+\@[a-z_.-]+\.[a-z]{2,4}$/i, "mail");
    })
})

document.addEventListener("keydown", (e) => {
    if (e.code === "Backspace" && e.target.id === "inputPhone4") {

        if (e.target.value.length === e.target.value.lastIndexOf(" ") + 1) {
            e.target.value = e.target.value.replace(/\s$/, "");         
        }
    }
})

//buttons

//зміна кольору кнопки при наведенні і натисканні
btns.forEach(elem => {
    elem.addEventListener("mousemove", (e) => {
        e.target.classList.add("mouse-move");
    })

    elem.addEventListener("mouseout", (e) => {
        e.target.classList.remove("mouse-move");
    })
})

//збереження даних
let saveInfo = () => {
    if (Object.values(userInfo).length < 4) {
        return;
    } else {
        let user = JSON.stringify(userInfo);
        sessionStorage.user = user;
        location.reload();
    }
}

//кнопка submit
let submit = (e) => { 
    inputs2.forEach(item => {
    validate(item, "inputName", /^[А-яЇїІіЄєЙй'-]+$/, "name");
    validate(item, "inputSname", /^[А-яЇїІіЄєЙй'-]+$/, "sname");
    validate(item, "inputPhone4", /\+380\d{2} \d{3} \d{2} \d{2}/, "tel");
    validate(item, "inputEmail4", /^[a-z_.-]+\@[a-z_.-]+\.[a-z]{2,4}$/i, "mail"); 
    }) 
    e.preventDefault();
    saveInfo();
}

//кнопка reset
let reset = () => {
    location.reload();
   // [...document.querySelectorAll("span")].forEach(item => {
   //     item.remove();
   // })
   // document.querySelector(".logo").remove();
   // document.querySelector("input[type='tel']").classList.remove("step-left");
}

btns[0].addEventListener("click", reset);
btns[1].addEventListener("click", submit);