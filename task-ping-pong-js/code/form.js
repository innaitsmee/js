const INPUT = document.querySelector("input[type='text']"),
BTN_MODAL = document.querySelector("input[type='button']"),
USER_NAME = document.querySelector(".user-name"),
COMPUTER_NAME = document.querySelector(".computer-name");

let hideModal = () => {
    START_MODAL.classList.remove("show-modal");
    START_MODAL.classList.add("close-modal");
},
    
showUserName = () => {
    if (INPUT.value) {
        //обмежуємо к-сть введених символів, щоб не виходити за межі верстки
        //userName.textContent = input.value.substr(0, 7);
        USER_NAME.textContent = INPUT.value;
        if (USER_NAME.textContent !== null) {
            document.querySelector(".user-name + span").textContent = ": ";
            COMPUTER_NAME.textContent = "Computer: ";
        }
        setTimeout(hideModal, 150);
        BTN_MODAL.removeAttribute("id", "error"); 
        BTN_MODAL.setAttribute("id", "success"); 
    } else {
        INPUT.blur();
        INPUT.setAttribute("id", "input-error-border");
        BTN_MODAL.setAttribute("id", "error");
        COMPUTER_NAME.textContent = "";
    }
};

INPUT.addEventListener("focus", () => {
    BTN_MODAL.removeAttribute("id", "error");
});

INPUT.addEventListener("change", () => {
    INPUT.removeAttribute("id", "input-error-border");
});

INPUT.addEventListener("keydown", (e) => {
    if ((e.code === "Enter" || e.code === "Tab")) {
        showUserName();
    }
});

BTN_MODAL.addEventListener("mouseover", () => {
    if (BTN_MODAL.id === "error") {
        if (INPUT.value) {
            BTN_MODAL.setAttribute("id", "mouse-move");
        } return;
    } else BTN_MODAL.setAttribute("id", "mouse-move");
});

BTN_MODAL.addEventListener("mouseout", () => {
    if (BTN_MODAL.id === "error") {
        if (INPUT.value) {
            BTN_MODAL.removeAttribute("id", "mouse-move");
        } return;
    } else BTN_MODAL.removeAttribute("id", "mouse-move");
});

BTN_MODAL.addEventListener("click", showUserName); 