const USER_PLATFORM = document.querySelector(".right"),
USER_PLATFORM_STYLE = getComputedStyle(USER_PLATFORM),
PLATFORM = document.querySelector(".platform"),
PLATFORM_STYLE = getComputedStyle(PLATFORM),
COMPUTER_PLATFORM = document.querySelector(".left"),
COMPUTER_PLATFORM_STYLE = getComputedStyle(COMPUTER_PLATFORM),
BALL = document.querySelector(".ball"),
BALL_STYLE = getComputedStyle(BALL),
GAME_AREA_STYLE = getComputedStyle(document.querySelector(".area"));

let round = {},
    userCounter = 0,
    computerCounter = 0;

let area = {
    top: parseFloat(GAME_AREA_STYLE.top),
    left: parseFloat(GAME_AREA_STYLE.left),
    height: parseFloat(GAME_AREA_STYLE.height),
    width: parseFloat(GAME_AREA_STYLE.width),
}

class Platform {
    constructor (top, left, height, width) {
        this.top = parseFloat(top);
        this.left = parseFloat(left);
        this.height = parseFloat(height);
        this.width = parseFloat(width);
        this.startPlatformPosition = parseFloat(top);
    }

    //рух платформи вгору
    movePlatformTop(platform) {
        if (this.top > 0) {
            this.top -= 10;
            platform.style.top = `${this.top}px`;
        }
    };

    //рух платформи вниз
    movePlatformBottom(platform) {
        if (this.top + this.height < area.height - 40) {
            this.top += 10;
            platform.style.top = `${this.top}px`;
        }
    };
}

class Ball {
    constructor(top, left, width, height) {
        this.top = parseFloat(top),
        this.left = parseFloat(left),
        this.width = parseFloat(width),
        this.height = parseFloat(height),
        this.startBallTop = parseFloat(top),
        this.startBallLeft = parseFloat(left),
        this.x = 2,
        this.y = 3;
    }

    bottomWall() {
        if (this.top + this.height >= area.height) {
            this.y = -this.y;
        }
    };

    rightWall() {
        if (this.left + this.width >= area.width) {
            this.x = -this.x;
        }
    };

    topWall() {
        if (this.top <= area.top) {
            this.y = -this.y;
        }
    };

    leftWall() {
        if (this.left <= area.left) {
            this.x = -this.x;
        }
    };

    move(ball) {
        this.top += this.y;
        this.left += this.x;
        ball.style.top = `${this.top}px`;
        ball.style.left = `${this.left}px`;
    };
}

//рух платформи комп*ютера
let moveComputerPlatform = () => {
    if (ball.left < computerPlatform.left + computerPlatform.width + 55) {

        if (computerPlatform.top + computerPlatform.height / 2 <
            ball.top + ball.height / 2) {
            computerPlatform.movePlatformBottom(COMPUTER_PLATFORM);

        } else computerPlatform.movePlatformTop(COMPUTER_PLATFORM);
    }  
}

//рух платформи гравця
document.addEventListener("keydown", (e) => {
    if (START_MODAL.classList.contains("close-modal") &&
        MAIN_BTN.textContent !== "Start") {
        
        //рух платформи вгору
        if (e.code === "ArrowUp") {
            userPlatform.movePlatformTop(USER_PLATFORM);
        }
        //рух платформи вниз
        if (e.code === "ArrowDown") {
            userPlatform.movePlatformBottom(USER_PLATFORM);
        }
    } 
});

//відбивання м*ячика гравцем
let hitBallByUser = () => {
    if (ball.top + ball.height > userPlatform.top &&
        ball.top < userPlatform.top + userPlatform.height &&
        ball.left + ball.width >= area.width - userPlatform.width - 9) {
        ball.x = -5;
    } 
},

//відбивання м*ячика комп*ютером
hitBallByComputer = () => {
    if (ball.top + ball.height > computerPlatform.top &&
        ball.top < computerPlatform.top + computerPlatform.height &&
        ball.left <= computerPlatform.left + computerPlatform.width) {
        ball.x = 5;
    }
};

//к-сть забитих м*ячів юзером
let countUser = () => {
    if (ball.left === -4) {
        document.querySelector(".user-count").textContent = `${++userCounter}`;
    }
},

//к-сть забитих м*ячів комп*ютером
countComputer = () => {
    if (ball.left + ball.width === area.width + 1) {
        document.querySelector(".computer-count").textContent = `${++computerCounter}`;
    }
};

let game = function (count) {
    if (computerCounter === count) {
        stopBall();
    } else {
    ball.move(BALL);
    ball.topWall();
    ball.bottomWall();
    ball.rightWall();
    ball.leftWall();
    moveComputerPlatform();
    hitBallByUser();
    hitBallByComputer();
    countUser();
    countComputer();
    } 
}

//рух м*ячика
let flyBall = () => {
    if (START_MODAL.classList.contains("close-modal") &&
        MAIN_BTN.textContent !== "Start") {
        
        if (restart === true) {
            ball.x = 2;
            ball.y = 3;
            ball.top = ball.startBallTop;
            ball.left = ball.startBallLeft;
            userPlatform.top = userPlatform.startPlatformPosition;
            restart = false;
        }

    document.querySelector(".computer-count").textContent = `${computerCounter}`;
    document.querySelector(".user-count").textContent = `${userCounter}`;
    game(5);
    }
}

let gameCount = setInterval(flyBall, 5);

//final
let stopBall = () => {
    clearInterval(gameCount);
    round.computer = computerCounter;
    round[USER_NAME.textContent] = userCounter;
    setTimeout(final, 100);
}

let final = function () {
    showResult();
    MAIN_BTN.textContent = "Start";

    BALL.style.top = `${ball.startBallTop}px`;
    BALL.style.left = `${ball.startBallLeft}px`;
    USER_PLATFORM.style.top = `${userPlatform.startPlatformPosition}px`;
    COMPUTER_PLATFORM.style.top = `${computerPlatform.startPlatformPosition}px`;
    
    [...document.querySelectorAll(".info-box")].forEach(e => {
        e.classList.add("hide-content"); 
    })
};

let userPlatform = new Platform(PLATFORM_STYLE.top, USER_PLATFORM_STYLE.left,
PLATFORM_STYLE.height, PLATFORM_STYLE.width),

computerPlatform = new Platform(PLATFORM_STYLE.top, PLATFORM_STYLE.left,
PLATFORM_STYLE.height, PLATFORM_STYLE.width),

ball = new Ball(BALL_STYLE.top, BALL_STYLE.left, BALL_STYLE.width, BALL_STYLE.height);