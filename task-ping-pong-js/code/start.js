const START_MODAL = document.querySelector(".modal"),
MAIN_BTN = document.querySelector(".start > span");

let click = false,
      
//модальне вікно, що з*являється при натиску кнопки старт    
showModal = () => {
    START_MODAL.classList.remove("close-modal");
    START_MODAL.classList.add("show-modal");
    document.querySelector("input[type='text']").focus();
};

//натискаємо start   
document.querySelector(".start").addEventListener("click", () => {
    if (click === false) {
        click = true;
        MAIN_BTN.textContent = "Finish";
        showModal();
    } else {
        MAIN_BTN.textContent = "Start";
        stopBall();
    }
});