const TITLE = document.querySelector(".result__title"),
RESULT_MODAL = document.querySelector(".result"),
RESULT_CONTENT = document.querySelector(".result__content");

let restart = false;

//таблиця результатів
let resultContent = function (computer, user) {

    let mainDiv = document.createElement("div"),
    computerDiv = document.createElement("div"),
    userDiv = document.createElement("div");
    
    mainDiv.classList.add("result__table");
    document.querySelector(".result__title + div").prepend(mainDiv);
    mainDiv.append(computerDiv);
    mainDiv.append(userDiv);
    computerDiv.textContent = `Computer: ${computer}`;   
    userDiv.textContent = `${Object.keys(user)}: ${Object.values(user)}`;
}

//заголовок таблиці
let resultTitle = (computer, user) => {
    TITLE.classList.remove("computer-win", "user-win", "green");
    TITLE.textContent = "";   

    if (computer > Object.values(user)[0]) {
        TITLE.textContent = `Computer win`;
        TITLE.classList.add("computer-win");

    } else if (computer < Object.values(user)[0]) {
        TITLE.textContent = `${Object.keys(user)} win`;
        TITLE.classList.add("user-win");

    } else {
        TITLE.textContent = `dead heat`;
        TITLE.classList.add("green");
    }
}

//виводимо результати
let showResult = () => {
    let { computer, ...user } = round;
    RESULT_MODAL.classList.remove("close-result");
    RESULT_MODAL.classList.add("show-result");
    RESULT_CONTENT.classList.remove("close-content");
    RESULT_CONTENT.classList.add("show-content"); 
    resultContent(round.computer, user);
    resultTitle(computer, user);
}

//сховати таблицю результатів
let hideResult = () => {
    RESULT_MODAL.classList.remove("show-result");
    RESULT_MODAL.classList.add("close-result");
    RESULT_CONTENT.classList.remove("show-content");
    RESULT_CONTENT.classList.add("close-content");
}

//почати нову гру
let startNewGame = () => {
    userCounter = 0;
    computerCounter = 0;
    restart = true;

    [...document.querySelectorAll(".info-box")].forEach(e => {
        e.classList.remove("hide-content"); 
    })

    MAIN_BTN.textContent = "Finish";
    gameCount = setInterval(flyBall, 5);
}

//нажимаємо close
document.querySelector(".close").addEventListener("click", () => {
    hideResult();
    setTimeout(() => {
        location.reload();
    }, 500)
})

//нажимаємо рестарт
document.querySelector(".restart").addEventListener("click", startNewGame);
document.querySelector(".restart").addEventListener("click", hideResult);

document.addEventListener("keydown", (e) => {
    if (RESULT_MODAL.classList.contains("show-result") &&
       (e.code === "Enter" || e.code === "Tab")) {
        hideResult();
        startNewGame();
    }
});