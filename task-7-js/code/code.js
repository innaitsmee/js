//1 - шибениця
let name = prompt("What's your name?");
console.log("Hello " + name);

let words = ["javascript", "monkey", "amazing", "pancake"];

let word = words[Math.floor(Math.random() * words.length)].split("");

let answerArray = [];

//for (let i = 0; i < word.length; i++) {
//    answerArray[i] = "_";
//}

word.forEach((item) => answerArray.push("_"));

let remainingLetters = word.length;

while (remainingLetters > 0) {

   alert(answerArray.join(" "));
   let guess = prompt("Guess a letter, or click Cancel to stop playing.");
   if (guess === null) {
       break;
   } else if (guess.length !== 1) {
       alert("Please enter a single letter")
   } else {
       //for (let j = 0; j < word.length; j++) {
       //    if (word[j] === guess) {
       //        answerArray[j] = guess;
       //        remainingLetters--;
       //    }
       //}

        word.forEach((item, i) => {
            if (item === guess) {
                answerArray[i] = guess;
                remainingLetters--;
            }
        })
    }
}
alert(answerArray.join(" "));
alert(`God job! The answer was ${word.join("")}`);

//2 - задача
//Реализовать функцию для создания объекта "пользователь".
//Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
//При вызове функция должна спросить у вызывающего имя и фамилию.
//Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
//Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя,
//соединенную с фамилией пользователя, все в нижнем регистре(например, Ivan Kravchenko → ikravchenko).
//Создать пользователя с помощью функции createNewUser().Вызвать у пользователя функцию getLogin().
//Вывести в консоль результат выполнения функции.

function User(name, sname) {
    this.firstName = name;
    this.lastName = sname;
};

User.prototype.getLogin = function () {
    return ((this.firstName[0] + this.lastName).toLowerCase());
};

User.createNewUser = function (name, sname) {
    let newUser = new User(name, sname);

    return newUser;
}


console.log(`login: ${User.createNewUser(prompt("name"), prompt("sname")).getLogin()}`);

//3 - задача
//Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
//Возьмите выполненное задание выше(созданная вами функция createNewUser()) и дополните ее следующим функционалом:
//При вызове функция должна спросить у вызывающего дату рождения(текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
//Создать метод getAge() который будет возвращать сколько пользователю лет.Создать метод getPassword(),
//который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией(в нижнем регистре)
//и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).Вывести в консоль результат работы функции
//createNewUser(), а также функций getAge() и getPassword() созданного объекта.

User.prototype.getAge = function () {
    let date = new Date();
    let birthDate = this["birthday"].replace(/\./g, "");

    let birthDay = birthDate.slice(0, 2);
    let birthMonth = birthDate.slice(2, 4);
    let birthYear = birthDate.slice(-4);

    let age;
    if (birthDay > "31" || birthMonth > "12") {
        console.error("!!! Date is undefined");
        this.birthday = undefined;
    };

    if (birthMonth <= (date.getMonth() + 1)) { // тут date.getMonth() + 1, бо місяці рахуються з 0;
        if (birthDay <= date.getDate()) {
            age = date.getFullYear() - birthYear;
        } else {
            age = date.getFullYear() - birthYear;
            age--;
        }                    
    } else {
        age = date.getFullYear() - birthYear;
        age--;
    }

    return age;
    
}

User.prototype.getPassword = function () {
    
    return ((this.firstName[0]).toUpperCase() + this["lastName"].toLowerCase() + this["birthday"].slice(-4));
}

User.createNewUser = function (name, sname, bday) {

   let newUser = new User(name, sname);

   newUser.birthday = bday;
    
   console.log(`age: ${newUser.getAge()}`); //не придумала як звернутись до створеного об*єкта зовні, тому викликала в функції createNewUser
   
   console.log(`password: ${newUser.getPassword()}`);

    return newUser;
}

console.log(User.createNewUser(prompt("name"), prompt("sname"), prompt("bday: dd.mm.yyyy")));





   
    