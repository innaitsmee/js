/*Реалізуйте програму перевірки телефону
* Використовуючи JS Створіть поле для введення телефону та кнопку збереження
* Користувач повинен ввести номер телефону у форматі 000-000-00-00
* Після того як користувач натискає кнопку зберегти перевірте правильність введення номера, 
якщо номер правильний зробіть зелене тло і використовуючи document.location переведіть користувача 
на сторінку https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg якщо буде помилка, 
відобразіть її в діві до input.*/


let flag = false,
    
    success = () => {
        document.body.classList.add("success-back");
        setTimeout(() => {
            document.location = "https://www.meme-arsenal.com/memes/01c6baa4ee457fac48ee2326c73dfc4e.jpg";
        }, 300);
    },

    addError = () => {
        let div = document.querySelector("body > div:first-child"),
            error = createElem("p");
    
        error.classList.add("error-text");
        error.textContent = "*Неправильний формат. Спробуйте ще раз.";
        div.after(error);
    },
    
    deleteError = () => {
        return document.querySelector("p").remove();
    };

function createElem(elem, value, type, placeholder) {
    let element = document.createElement(elem);

    if (value !== "") {
        element.value = value;
    };

    if (type !== "") {
        element.type = type;
    };

    if (placeholder !== "") {
        element.placeholder = placeholder;
    };

    return element;
}

function createButtons() {
    const div = createElem("div"),
          input = createElem("input", "", "tel", "000-000-00-00"),
          btn = createElem("input", "Save", "button");
    
    document.querySelector(".container-stopwatch").after(div);
    div.append(input);
    input.after(btn);
}

function checkPhone() {
    let input = document.querySelector("div > input:first-child"),
        pattern = /\d{3}-\d{3}-\d{2}-\d{2}/;
    
    if (pattern.test(input.value)) {
        if (flag === true) {
        deleteError(); 
        }
        success();
    } else {
        if (flag === false) {
        input.classList.add("error-input");
        addError();
        }
        flag = true;
    }
}

document.onload = createButtons();
document.querySelector("div > input:last-child").onclick = checkPhone;



    