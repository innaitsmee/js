/*Слайдер
Створіть слайдер кожні 3 сек змінюватиме зображення
Зображення для відображення
https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg
https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg
https://naukatv.ru/upload/files/shutterstock_418733752.jpg
https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg
https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg*/


let next = 0,
    offset = 0,
    slides = [],
    src = ['https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg',
    'https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg',
    'https://naukatv.ru/upload/files/shutterstock_418733752.jpg',
    'https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg',
    'https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg'],
    container = document.createElement("div");
    document.querySelector("body > div:nth-child(2)").after(container);
    container.classList.add("slide-box");

let widthImage = () => {
    let width = getComputedStyle(container).width,
        [widthImage] = width.match(/\d{1,}/);
    return widthImage;
    },
    
    startPosition = (number) => {
        let startPosition = `${number * widthImage()}px`;
        return startPosition;
    },
    
    newPosition = (number) => {
        let newPosition = `${(number * widthImage() - widthImage())}px`;
        return newPosition;
    },

    createSlide = function() {
        let image = document.createElement("img");
        image.classList.add("slide");
        image.src = src[next];
        image.style.left = startPosition(offset);
        container.append(image);
        offset = 1;
        if (next === (src.length - 1)) {
            next = 0;
        } else next++;
    },
    
    moveSlide = function () {
        let offset2 = 0;
        [...slides] = document.querySelectorAll(".slide");
        
        slides.forEach(e => {
            e.style.left = newPosition(offset2);
            offset2++;
        })
        deleteSlide();
    },

    deleteSlide = function() {
        setTimeout(() => {
            slides[0].remove();
            createSlide();
        }, 2000);
    };

    setInterval(moveSlide, 3000);

createSlide();
createSlide();