/*Створіть програму секундомір.
* Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
* При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий 
* Виведення лічильників у форматі ЧЧ:ММ:СС
* Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції
*/


const btnStart = document.querySelector("#start"),
      btnStop = document.querySelector("#stop"),
      btnReset = document.querySelector("#reset"),
      stopWatch = document.querySelector(".container-stopwatch"),
      secWatch = document.querySelector(".sec"),
      minWatch = document.querySelector(".min"),
      hourWatch = document.querySelector(".hour");

let seconds = 0,
    minutes = 0,
    hours = 0,
    click = false,
    interval,

    clearClass = () => {
        stopWatch.classList.remove("black", "red", "green", "silver");
    },

    showTime = () => {
        showSeconds();
        showMinutes();
        showHours();
    },

    showSeconds = () => {
        if (seconds < 10) {
            secWatch.textContent = `0${seconds}`;
        } else secWatch.textContent = seconds;
    },

    showMinutes = () => {
        if (minutes < 10) {
            minWatch.textContent = `0${minutes}`;
        } else minWatch.textContent = minutes;
    },

    showHours = () => {
        if (hours < 10) {
            hourWatch.textContent = `0${hours}`;
        } else hourWatch.textContent = hours;
    },

    stop = () => {
        clearInterval(interval);
    },

    reset = () => {
        seconds = 0;
        minutes = 0;
        hours = 0;
        showSeconds();
        showMinutes();
        showHours();
    };

function start() { //не робила стрілкову, бо сильно багато коду як для стрілкової

    interval  = setInterval(() => {

        if (hours === 99) {
            seconds = 0;
            minutes = 0;
        } else {
            seconds++;

            if (seconds > 59) {
                seconds = 0;
                minutes++;

                if (minutes > 59) {
                    minutes = 0;
                    hours++;
                };
            };
        }
        showTime();
    }, 1000)
}

btnStart.onclick = () => {
    clearClass();
    stopWatch.classList.add("green");
    if (!click) {
        start();
        click = true;
    };
}

btnStop.onclick = () => {
    clearClass();
    stopWatch.classList.add("red");
    stop();
    click = false;
}

btnReset.onclick = () => {
    clearClass();
    stopWatch.classList.add("silver");
    reset();
    click = false;
}