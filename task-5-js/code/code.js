//Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата".
//Создать в объекте вложенный объект - "Приложение". Создать в объекте "Приложение", вложенные объекты,
//"Заголовок, тело, футер, дата". Создать методы для заполнения и отображения документа.

let doc = {
    header:"",
    body:"",
    footer:"",
    data:"",
    app: {

        header: {},
        body: {},
        footer: {},
        data: {},
        
        textApp: function () {

            for (let el in doc.app) {
            
                if (doc.app[el] === doc.app.textApp) {
                    break;
                }

              //  doc.app[el].textApp = prompt("text here");

                document.getElementById("app").innerHTML += `<div> ${doc.app[el].textApp} </div>`;

        }

    }
    },
   
    text: function () {

        for (let el in doc) {

            if (typeof (this[el]) === "string") {

             //   this[el] = prompt("text here");

                document.getElementById(el).innerHTML = this[el];

            }
        }

    }

}

doc.text();

doc.app.textApp();


//Создайте объект криптокошилек. В кошельке должно хранится имя владельца, несколько валют Bitcoin, Ethereum,
//Stellar и в каждой валюте дополнительно есть имя валюты, логотип, несколько монет и курс на сегодняшний
//день.
//Также в объекте кошелек есть метод при вызове которого он принимает имя валюты и выводит на страницу
//информацию.
//"Добрый день, ... ! На вашем балансе (Название валюты и логотип) осталось N монет, если вы сегодня продадите
//их то, получите ...грн.

let wallet = {
    name: prompt("text your name"),
    bitcoin: {
        name: "Bitcoin",
        logo: "<img src = 'https://s2.coinmarketcap.com/static/img/coins/64x64/1.png'>",
        value: 3,
        rateUAH: 831477.73
        
    },

    ethereum: {
        name: "Ethereum",
        logo: "<img src = 'https://s2.coinmarketcap.com/static/img/coins/64x64/1027.png'>",
        value: 5,
        rateUAH: 58809.89

    },
    
    stellar: {
        name: "Stellar",
        logo: "<img src = 'https://s2.coinmarketcap.com/static/img/coins/64x64/512.png'>",
        value: 10,
        rateUAH: 4.17
    },

    showInfo: function (nameCoin) {

        let coin = nameCoin.toLowerCase();

        if (coin === "btc") {

            return wallet.message("bitcoin");
  
        }

        else if (coin === "eth") {

           return wallet.message("ethereum");
  
        }
            
        else if (coin === "xlm") {

            wallet.message("stellar")

        }
                  
        else if (coin !== "btc" && coin !== "eth" && coin !== "xlm" && coin !== "bitcoin" && coin !== "ethereum" && coin !== "stellar") {
            alert("ERROR! Wrong name coin. Try again.");
        }    
    
        else {
            return wallet.message(coin);
        }

    },

    message: function (coin) {

        return `Добрый день, ${wallet.name}! На вашем балансе ${wallet[coin].name} ${wallet[coin].logo}
        осталось ${wallet[coin].value} монет(ы), если вы сегодня продадите их, то 
        получите ${(wallet[coin].value * wallet[coin].rateUAH).toFixed(2)} грн.`

    }

}

document.getElementById("wallet").innerHTML = wallet.showInfo(prompt("text name coin"));






