  /*
             * Класс, объекты которого описывают параметры гамбургера.
             *
             * @constructor
             * @param size        Размер
             * @param stuffing    Начинка
             * @throws {HamburgerException}  При неправильном использовании
             */
        // function Hamburger(size, stuffing) { ... } 

        function Hamburger (size, stuffing) {

                try {
                    
                    if (size.name === "Large" || size.name === "Small") {
                        this.size = size.name;
                    } else {
                        throw new HamburgerException;
                    };   
                   
                    if (stuffing.name === "Chesse" || stuffing.name === "Salad" || stuffing.name === "Potato") {
                         this.stuffing = stuffing.name;
                    } else {
                         throw new HamburgerException;
                    }


                } catch(err) { 

                    if (!size) {
                                throw new HamburgerException("error. no size given");
                        }

                    else if (this.size !== "Large" && this.size !== "Small") {
                                throw new HamburgerException("error. invalid size");
                        };  

                    if (!stuffing) {
                                throw new HamburgerException("error. no stuffing given");
                        }  
    
                    else if (this.stuffing !== "Chesse" && this.stuffing !== "Salad" && this.stuffing !== "Potato") {
                                throw new HamburgerException("error. invalid stuffing");
                        };
                }   
                }
        

         /* Размеры, виды начинок и добавок */
         Hamburger.SIZE_SMALL = {name: "Small", priceUAH: 50, calories: 20};
         Hamburger.SIZE_LARGE = {name: "Large", priceUAH: 100, calories: 40};
         Hamburger.STUFFING_CHEESE = {name: "Chesse", priceUAH: 10, calories: 20};
         Hamburger.STUFFING_SALAD = {name: "Salad", priceUAH: 20, calories: 5};
         Hamburger.STUFFING_POTATO = {name: "Potato", priceUAH: 15, calories: 10};
         Hamburger.TOPPING_MAYO = {name: "Mayo", priceUAH: 20, calories: 5};
         Hamburger.TOPPING_SPICE = {name: "Spice", priceUAH: 15, calories: 0};


        /*
         * Добавить добавку к гамбургеру. Можно добавить несколько
         * добавок, при условии, что они разные.
         *
         * @param topping     Тип добавки
         * @throws {HamburgerException}  При неправильном использовании
         */
        // Hamburger.prototype.addTopping = function (topping) ...
//
        Hamburger.prototype.toppingList = [];  //в функції масив при кожному виклику ф-ції робився порожнім і не можна було записати туди додатвкові дані
        //тому винесла сюди

        Hamburger.prototype.addTopping = function (topping) {

               if (arguments.length === 1) {

                try {
                        if (this.toppingList.length > 0) {

                                for (let i = 0; i < this.toppingList.length; i++) {
                                        if (this.toppingList[i] !== topping.name) {
                                                this.toppingList.push(topping.name);
                                        } else throw new HamburgerException;
                                }
                        } else {

                                this.toppingList = [topping.name];
                        }

                } catch (err) {
                        throw new HamburgerException("duplicate topping");
                }

                        
                }

                else if (arguments.length > 1) 
                       for (let i = 0; i < (arguments.length - 1); i++) {
                               if (arguments[i] === arguments[i + 1]) {
                                       this.toppingList[i] = arguments[i].name;
                               }
                               else {
                                       this.toppingList[i] = arguments[i].name;
                                       this.toppingList[i + 1] = arguments[i + 1].name;
                               }

                }

               }

        // /**
        //  * Убрать добавку, при условии, что она ранее была 
        //  * добавлена.
        //  * 
        //  * @param topping   Тип добавки
        //  * @throws {HamburgerException}  При неправильном использовании
        //  */
        // Hamburger.prototype.removeTopping = function (topping) ...
           
         Hamburger.prototype.removeTopping = function (topping) {
                
                for (let i = 0; i < this.toppingList.length; i++) {
                        if (this.toppingList[i] === topping.name) {
                               this.toppingList.splice (i, 1);
                        }
                }
         }

         
        // /**
        //  * Получить список добавок.
        //  *
        //  * @return {Array} Массив добавленных добавок, содержит константы
        //  *                 Hamburger.TOPPING_*
        //  */
        // Hamburger.prototype.getToppings = function () ...
  
        Hamburger.prototype.getToppings = function () {
                return this.toppingList;
        }


        // /**
        //  * Узнать размер гамбургера
        //  */
        // Hamburger.prototype.getSize = function () ...

        Hamburger.prototype.getSize = function () {
                return this.size;
        }

        // /**
        //  * Узнать начинку гамбургера
        //  */
        // Hamburger.prototype.getStuffing = function () ...

        Hamburger.prototype.getStuffing = function () {
                return this.stuffing;
        }

         /**
          * Узнать цену гамбургера
          * @return {Number} Цена в тугриках
          */
        // Hamburger.prototype.calculatePrice = function () ...

        Hamburger.prototype.calculatePrice = function () { //скоріше всього можна зробити простіше, але не придумала як

        let sizePrice = function (size) { //можливо краще було б винести ці маленькі ф-ції, але не хотіла порушувати структуру яка була вказана в jsDoc
                if (size === "Small") {
                        return Hamburger.SIZE_SMALL.priceUAH;
                } else {
                        return Hamburger.SIZE_LARGE.priceUAH;
                };
        }
        let stuffingPrice = function (stuffing) {
                if (stuffing === "Chesse") {
                        return Hamburger.STUFFING_CHEESE.priceUAH;
                } else if (stuffing === "Salad") {
                        return Hamburger.STUFFING_SALAD.priceUAH;
                } else {
                        return Hamburger.STUFFING_POTATO.priceUAH;
                };
        }
        let toppingPrice = function (topping) {
                if (topping.length > 1) {
                        return Hamburger.TOPPING_MAYO.priceUAH + Hamburger.TOPPING_SPICE.priceUAH;
                } else {
                        if (topping[0] === "Mayo") {
                                return Hamburger.TOPPING_MAYO.priceUAH;
                        } else {
                                return Hamburger.TOPPING_SPICE.priceUAH;
                        }
                }
        }

        let rateMNT = 87.75;

        //ціна в тугриках

        let resPrice = (sizePrice(this.getSize()) + stuffingPrice(this.getStuffing()) + toppingPrice(this.getToppings())) * rateMNT;

        return resPrice;

        }

        // /**
        //  * Узнать калорийность
        //  * @return {Number} Калорийность в калориях
        //  */
        // Hamburger.prototype.calculateCalories = function () ...

        Hamburger.prototype.calculateCalories = function () {

        let sizeCalories = function (size) {
                if (size === "Small") {
                        return Hamburger.SIZE_SMALL.calories;
                } else {
                        return Hamburger.SIZE_LARGE.calories;
                };
        }
        let stuffingCalories = function (stuffing) {
                if (stuffing === "Chesse") {
                        return Hamburger.STUFFING_CHEESE.calories;
                } else if (stuffing === "Salad") {
                        return Hamburger.STUFFING_SALAD.calories;
                } else {
                        return Hamburger.STUFFING_POTATO.calories;
                };
        }
        let toppingCalories = function (topping) {
                if (topping.length > 1) {
                        return Hamburger.TOPPING_MAYO.calories + Hamburger.TOPPING_SPICE.calories;
                } else {
                        if (topping[0] === "Mayo") {
                                return Hamburger.TOPPING_MAYO.calories;
                        } else {
                                return Hamburger.TOPPING_SPICE.calories;
                        }
                }
        }

        let resCalories = sizeCalories(this.getSize()) + stuffingCalories(this.getStuffing()) + toppingCalories(this.getToppings());
        
        return resCalories;

        }

        // /**
        //  * Представляет информацию об ошибке в ходе работы с гамбургером. 
        //  * Подробности хранятся в свойстве message.
        //  * @constructor 
        //  */
        // function HamburgerException (...) { ... }

        function HamburgerException(message) {
           this.message = message;
           this.name = "HamburgerException";
        }
        
      // Комментарии:
      //  - Это задача на ООП.Вам нужно сделать класс, который получает на вход информацию о гамбургере, 
      //  и на выходе дает информацию о весе и цене.Никакого взаимодействия с пользователем и внешним миром 
      //  класс делать не должен - все нужные данные ему необходимо передать явно.Он ничего не будет спрашивать, 
      //  и не будет ничего выводить.
      //  - Почему ? Потому что каждый должен заниматься своим делом, класс должен только обсчитывать гамбургер, 
      //  а вводом - выводом должны заниматься другие части кода.Иначе мы получим кашу, где разные функции смешаны вместе.
      //  - Типы начинок, размеры надо сделать константами.Никаких магических строк не должно быть.
      //  - Переданную информацию о параметрах гамбургера экземпляр класса хранит внутри в своих полях.
      //  Вот как может выглядеть использование этого класса:

       // маленький гамбургер с начинкой из сыра
      // var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
      // // добавка из майонеза
      // hamburger.addTopping(Hamburger.TOPPING_MAYO);
      // // спросим сколько там калорий
      // console.log("Calories: %f", hamburger.calculateCalories());
      // // сколько стоит
      // console.log("Price: %f", hamburger.calculatePrice());
      // // я тут передумал и решил добавить еще приправу
      // hamburger.addTopping(Hamburger.TOPPING_SPICE);
      // // А сколько теперь стоит? 
      // console.log("Price with sauce: %f", hamburger.calculatePrice());
      // // Проверить, большой ли гамбургер? 
      // console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
      // // Убрать добавку
      // hamburger.removeTopping(Hamburger.TOPPING_SPICE);
      // console.log("Have %d toppings", hamburger.getToppings().length); // 1


      //При неправильном использовании класс сообщает об этом с помощью выброса исключения.
      // не передали обязательные параметры
      // var h2 = new Hamburger(); // => HamburgerException: no size given
//
      //  // передаем некорректные значения, добавку вместо размера
       // var h3 = new Hamburger(Hamburger.TOPPING_MAYO, Hamburger.TOPPING_SPICE);
      //  // => HamburgerException: invalid size 'TOPPING_SAUCE'
//
      //  // добавляем много добавок
       // var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
       // hamburger.addTopping(Hamburger.TOPPING_MAYO);
       // hamburger.addTopping(Hamburger.TOPPING_MAYO);
      //   // HamburgerException: duplicate topping 'TOPPING_MAYO'

     // В коде выше обратите внимание на такие моменты:

     //класс не взаимодействует с внешним миром.Это не его дело, этим занимается другой код, а класс живет в изоляции от мира
     //обязательные параметры(размер и начинка) мы передаем через конструктор, чтобы нельзя было создать объект, не указав их
     //        необязательные(добавка) добавляем через методы
     //имена методов начинаются с глагола и имеют вид «сделайЧтоТо»: calculateCalories(), addTopping()
     //типы начинок обозначены "константами" с понятными именами
     //об исключительных ситуациях сообщаем с помощью исключений
     //объект создается через конструктор - функцию, которая задает начальные значения полей.Имя конструктора пишется с большой буквы 
     //и обычно является существительным: new Hamburger(...)
     //"константы" вроде Hamburger.SIZE_SMALL могут иметь значение, являющееся строкой или числом.От смены значения константы ничего 
     //не должно меняться(то есть эти значения не должны где - то еще быть записаны).К сожалению, в отличие от 
     //других языков(Java, PHP, C#) при опечатке в имени такой "константы" интепретатор JS не укажет на ошибку
     //в свойствах объекта гамбургера логично хранить исходные данные(размер, тип начинки), но не хранить 
     //вычисляемые значения(цена, число калорий и т.д.).Рассчитывать цену и калории логично в тот момент, 
     //когда это потребуется, а не заранее.
     //в JS нет синтаксиса, чтобы пометить свойство или метод приватным(доступным для использования только внутри класса), 
     //потому некоторые разработчики начинают их имена с подчеркивания и договариваются, что извне класса к ним никто не обращается.
     //Вообще, в JS до версии ES6 нет нормальных классов, потому многое основано на таких договоренностях.
     //В дополнение, вот еще инструкция, как решать задачи на ООП.Когда ты решаешь задачу на ООП, ты должен ответить на вопросы:
     //какие есть сущности, для которых мы сделаем классы ? (Гамбургер).
     //какие у них есть свойства(размер, начинка, добавки).Цена или калории не являются свойствами так как они вычисляются 
     //из других свойств и хранить их не надо.
     //что мы хотим от них получить(какие у них должны быть методы).Например, сколько стоит гамбургер ?
     //            как сущности связаны ? У нас одна сущность «Гамбургер» и она ни с чем не связана.
     //Заметьте также, что в примере выше класс не взаимодействует с внешним миром.За это отвечает внешний относительно него код.
     //Потому наш класс унивесален: его можно использовать в консоли, выводя данные через console.log, а можно приделать навороченный 
     //HTML - интерфейс с кнопками для запуска на планшете с тачскрином.Именно в таком стиле необходимо писать ООП код.
     //Классы в JS имитируются всякими костылями разными споcобами, самый общеупотребимый - через добавление методов в прототипы объекта.
